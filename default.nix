{ ... }:

let
  fix = f: let x = f x; in x;
  readTree = import ./nix/readTree {};

  # Tracking nixpkgs/nixos-unstable as of 2020-12-29.
  nixpkgsCommit = "2f47650c2f28d87f86ab807b8a339c684d91ec56";
  nixpkgsSrc = fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${nixpkgsCommit}.tar.gz";
    sha256 = "17akl75x28rzq97gaad32flswdsp150nfsg7h909kda721zql71a";
  };
  nixpkgs = import nixpkgsSrc {};

in fix (self: rec {
  config = {
    inherit nixpkgsSrc nixpkgs;
    inherit (nixpkgs) pkgs lib stdenv;
    fediventure = self;
    root = ./.;
  };
  nix = readTree config ./nix;
  ops = readTree config ./ops;
  pages = readTree config ./pages;
  third_party = readTree config ./third_party;
})
