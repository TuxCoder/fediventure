# WorkAdventure packaging effort by SuperSandro2000, not yet upstreamed into nixpkgs.

{ fediventure, lib, pkgs, ... }:

let
  src = pkgs.fetchFromGitHub {
    owner = "SuperSandro2000";
    repo = "workadventure-nix";
    rev = "c5342ed3feda6f6f427fdea65a24f1ac86b3c2cc";
    sha256 = "0f4pxif8hc2v7zn1pxx4qgx93asj0rspkdnri56n9vc9pvg1hc5f";
  };

  # Use a fixed-point operator to build a nixpkgs-like structure that contains all
  # workadventure derivation.
  wapkgs = lib.fix (self: let
    callPackage = lib.callPackageWith (pkgs // self);
  in {
    workadventure-pusher = callPackage "${src}/pusher" {};
    workadventure-back = callPackage "${src}/back" {};
    workadventure-front = callPackage "${src}/front" {};
    workadventure-messages = callPackage "${src}/messages" {};
    workadventure-maps = callPackage "${src}/maps" {};
  });

# Build public attrset of all accessible components.
in rec {
  pusher = wapkgs.workadventure-pusher;
  back = wapkgs.workadventure-back;
  front = wapkgs.workadventure-front;
  maps = wapkgs.workadventure-maps;

  # Target for CI test.
  # There doesn't seem to be any built-in self-test within WA binaries, so
  # we just test for file presence for now.
  ci = fediventure.nix.mkTest  {
    name = "workadventure-build-test";
    buildCommand = ''
      set -e -x
      test -e ${pusher}/bin/workadventurepusher
      test -e ${back}/bin/workadventureback
      test -e ${front}/dist/index.html
      test -e ${maps}/workadventuremaps/Floor0/floor0.json
      mkdir $out
    '';
  };
}
