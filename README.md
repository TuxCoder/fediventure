Fediventure
===========

This is a repository to coordinate development and infrastructure around a federated Work Adventure for cross-*space communication.

Usage
-----

This repository is built around Nix. You will need to have it installed on your existing Linux machine, or even better - run NixOS :).

We use `readTree` to allow building of arbitrary nix derivations within this repository. For instance, if you see a file named `foo/bar.nix`, you can build its to-level derivation via `nix-build -A foo.bar` from the root of this repository. See [nix/readTree](nix/readTree) for more information.


License
-------

Unless otherwise noted, contents of this repository are licensed under the GNU Affero General Public License 3.0. See the COPYING file for more information.

Components
==========

Pages
-----

`pages/` contains infrastructure to deploy static HTML pages via GitLab pages, currently fediventure.net. See [pages/README.md](pages/README.md) for more information on how to edit and build the site.
