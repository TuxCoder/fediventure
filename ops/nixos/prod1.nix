{ config, pkgs, ... }:

{
  imports = [
    ./modules/vm-kurdebele.nix
  ];
  networking.hostName = "prod1";
  networking.interfaces.enp1s0 = {
    ipv6.addresses = [
      { address = "2a0f:4f9:4a:4319:1337::11"; prefixLength = 80; }
    ];
    ipv4.addresses = [
      { address = "135.181.235.219"; prefixLength = 29; }
    ];
  };
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/e6d56d40-d5f8-47e2-a0ae-00f97c81c61a";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/7A58-01E9";
      fsType = "vfat";
    };
  };
  swapDevices = [
    { device = "/dev/disk/by-uuid/075008d6-3ce6-48af-a0c0-c2f8d4ead582"; }
  ];
  system.stateVersion = "20.09";
}
