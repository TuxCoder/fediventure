{ fediventure, lib, pkgs, ... }:

# Top-level admins definitions.
# After adding yourself ensure that the file isn't broken by running the
# following in fediventure:
#
#    nix repl default.nix
#    nix-repl> :p ops.admins.humans.<you>
#
#    eg
#
#    nix-repl> :p ops.admins.humans.q3k
#
# You can also evaluate larger scopes, like:
#
#    nix-repl> ops.admins.sshkeys ops.admins.groups.all
#    nix-repl> :p ops.admins.humans
#
# (:p does a recursive print)

with builtins; rec {
  humans = {
    q3k = {
      sshkeys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG599UildOrAq+LIOQjKqtGMwjgjIxozI1jtQQRKHtCP"
      ];
    };
  };

  groups = {
    all = humans;
    rootOnProd = {
      inherit (humans) q3k;
    };
  };

  # SSH key fingerprints for a given group (like all, rootOnProd).
  sshkeys = group: lib.lists.flatten (lib.mapAttrsToList (n: p: (map (k: "${k} ${n}") p.sshkeys)) group);
  # Derivation that creates an authorized_keys file of SSH keys for a given group.
  authorizedKeys = group: builtins.toFile "authorized_keys" ((concatStringsSep "\n" ((sshkeys group) ++ [""])));

  test = fediventure.nix.mkTest {
    name = "admins";
    buildCommand = ''
      while read l; do
        ${pkgs.openssh}/bin/ssh-keygen -l -f /dev/stdin <<<$l >> log
      done < "${authorizedKeys groups.all}"
      mkdir $out
      cp log $out/log
    '';
  };
}
