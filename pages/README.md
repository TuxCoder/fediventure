fediventure.net
===============

To edit the page, run from the root of the repository:

    nix-build -A pages.fediventure-net.server
    result/bin/fediventure.net-devserver

This will spawn a local web server on port 5000. You can now edit content in pages/fediventure-net, and send a merge request with your changes.

Once your changes are merged to `main`, they will appear on fediventure.net.
